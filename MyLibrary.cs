﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson24_HW
{
    class MyLibrary 
    {
        private Dictionary<string, Book> books;

        public MyLibrary()
        {
            books = new Dictionary<string, Book>();
        }

        public bool AddBook(Book book)
        {
            if (!books.ContainsKey(book.Title))
            {
                books.Add(book.Title, book);
                return true;
            }
            return false;
        }

        public bool RemoveBook(string book)
        {
            if (books.TryGetValue(book, out Book value))
            {
                books.Remove(book);
                return true;
            }
            return false;
        }

        public bool HaveThisBook(string book)
        {
            return books.TryGetValue(book, out Book value);
        }

        public Book GetBook(string book)
        {
            if (books.TryGetValue(book, out Book value))
            {
                return value;
            }
            return null;
        }

        public Book GetBookByAuthor(string bookAuthor)
        {
            foreach (Book book in books.Values)
            {
                if (book.Author == bookAuthor)
                {
                    return book;
                }
            }
            return null;
        }

        public void Clear()
        {
            books.Clear();
        }

        public List<string> GetAuthor()
        {
            List<string> authors = new List<string>();
            foreach (Book item in books.Values)
            {
                authors.Add(item.Author);
            }
            return authors;
        }

        public List<Book> GetBokksStoredByAuthorName()
        {
            List<Book> book = new List<Book>();
            foreach (Book b in books.Values)
            {
                book.Add(b);
            }
            book.Sort();
            return book;
        }

        public List<string> GetBookTitleStored()
        {
            List<string> title = new List<string>();
            foreach (Book item in books.Values)
            {
                title.Add(item.Title);
            }
            title.Sort();
            return title;
        } 

        public int Count
        {
            get
            {
                return books.Count();
            }
        }

        public override string ToString()
        {
            string print = "=============Books in Library=============\n";
            foreach (KeyValuePair<string, Book> item in books)
            {
                print += "\n";
                print += $"[Key: {item.Key}]\nValue: {item.Value}\n";
            }
            return print;

        }
    }
}
