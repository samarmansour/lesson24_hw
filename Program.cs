﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson24_HW
{
    class Program
    {
        static void Main(string[] args)
        {
            Book b1 = new Book
                ("Pet Sematary",
                "The couple soon discover a mysterious burial ground hidden deep in the woods near....", 
                "Stephen King", "Horror Fiction");

            Book b2 = new Book
                ("The Fault in our stars",
                "Two cancer-afflicted teenagers Hazel and Augustus meet at a cancer support group....." 
                ,"John Green","Romance/Drama");

            Book b3 = new Book
                ("Exit", "A rock climber tries to save the day when a mysterious " +
                "white gas envelops an entire district in Seoul, South Korea.", 
                "Lee Sang-geun", "Comedy/Action");

            MyLibrary myLibrary = new MyLibrary();
            myLibrary.AddBook(b1);
            myLibrary.AddBook(b2);
            myLibrary.AddBook(b3);

            Console.WriteLine(myLibrary);

            myLibrary.RemoveBook("Exit");
            Console.WriteLine(myLibrary);

            Console.WriteLine(myLibrary.HaveThisBook("The Conquest of Andalusia"));

            myLibrary.GetBookTitleStored();
            Console.WriteLine(myLibrary);

            




        }
    }
}
